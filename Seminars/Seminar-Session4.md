# Session 4: An overview of DH projects

## Links discussed in class

### Projects mentioned:

* Venice Time Machine:
    - [website](https://vtm.epfl.ch/)
    - [flagship app](http://timemachineproject.eu/)
    - [a video presentation](https://www.youtube.com/watch?v=2-Ev4rU27HY)
    - [a short video](https://www.youtube.com/watch?v=QTBkuyFbIz0)
* [Pelagios](http://commons.pelagios.org/) and [Peripleo](http://pelagios.org/peripleo/map)
* [Mapping Metaphor with the Historical Thesaurus: Metaphor Map of English](http://mappingmetaphor.arts.gla.ac.uk)

### DH Awards
* http://dhawards.org/

### Conference proceedngs and series
* [abstracts](https://dh2017.adho.org/program/abstracts/) of the 2017 edition of the "Digital Humanities" conference (Montreal, QB, August 8-11, 2017)
* if you're interested in the Ancient World, have a look at the Digital Classicist series of [London](http://www.digitalclassicist.org/wip/) and [Berlin](http://de.digitalclassicist.org/berlin/) (with videos/screencasts)


### Papers and blog posts
* A generator of Socratic dialogues: [blog post](http://jonreeve.com/2016/10/socratic-dialogue-generator/), [GitHub repository](https://github.com/JonathanReeve/plato-analysis/blob/master/plato-characters-Markov.ipynb)
* [Network analysis - Digital Humanities on Twitter, a small-world?](http://www.martingrandjean.ch/digital-humanities-on-twitter/)
* Lev Manovich. ["How to Compare One Million Images?"](http://softwarestudies.com/cultural_analytics/2011.How_To_Compare_One_Million_Images.pdf) In David Berry, ed., *Understanding Digital Humanities* (Palgrave, 2012)


### To know more
* Linked Open Data:
    - page on the [W3C](https://www.w3.org/standards/semanticweb/data)
* Citizen Science:
    - good article on [Wikipedia](https://en.wikipedia.org/wiki/Citizen_science)
    - ["green paper"](https://ec.europa.eu/digital-single-market/en/news/green-paper-citizen-science-europe-towards-society-empowered-citizens-and-enhanced-research) of the European commission
    - homepage of the [Citizen Science Association](http://citizenscience.org/)