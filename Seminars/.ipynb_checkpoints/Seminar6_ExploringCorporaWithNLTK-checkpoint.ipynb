{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The [*Natural Language Toolkit*](http://www.nltk.org/) is a huge suite of Python's libraries and \"wrappers\" (i.e. Python functions that call software written in other languages).\n",
    "\n",
    "It contains an extensive collections of **resources** (corpora, dictionaries, word lists) and **software** that can be used to perform several Natural Language Processing tasks."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NLTK is constantly developed (now only in Python 3) and it includes a growing number of functionality. Its main strenghts are:\n",
    "\n",
    "* the existence of various **Corpus Readers** that help you to load many supported corpora on the fly\n",
    "* the built-in functions of several modules, that already support some important functions to explore and work with corpora\n",
    "* its extensibility: it is easy to load your own corpus by using the existing corpus readers or even building *your own* reader"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you're interesting in ancient languages (such Latin, Coptic or Sanscrit) or any other historic languages (Classical Arabic, Old English), you might be interested to know that there is a sort of \"spin off\" of NLTK called CLTK ([Classical Language Toolkit](http://cltk.org/))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## How to get started"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first thing you need to do is install NLTK. NLTK does **not** come preinstalled in Anaconda and it was not installed in your VM for a mere mistake on my side..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Install it by running (even from Jupyter!):\n",
    "\n",
    "```bash\n",
    "pip install -U nltk\n",
    "```\n",
    "\n",
    "(note: if you want to install it from Jupyter copy the command above in a code cell; **put an exclamation mark (!) before the word pip**.\n",
    "\n",
    "If you're installing it in your own machine and want to use a different package manager (e.g. `aptitude`) or want to install it for all users, feel free to refer to your system's installation guides"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The command above will just install the bare bones of NLTK. Many additions (e.g. the corpora, the tokenizers, etc...) will have to be installed singularly.\n",
    "\n",
    "NLTK provides even a Graphical User Interface to download and install components. If you want to start it: \n",
    "* start a python interpreter\n",
    "* import nltk\n",
    "    ```python\n",
    "    import nltk\n",
    "    \n",
    "    ```\n",
    "* type:\n",
    "    ```python\n",
    "    nltk.download()\n",
    "    \n",
    "    ```\n",
    "* for this session we will need the collection with the identifier `book`\n",
    "* modules and data of NLTK are installed from the command line using this command:\n",
    "    ```python\n",
    "    python -m nltk.downloader book\n",
    "    \n",
    "    ```\n",
    "* finally, you can also install modules from the interactive interpreter / the notebook, as in the two following cells\n",
    "    \n",
    "Refer to [this guide](http://www.nltk.org/data.html) for a complete instruction on how to install modules "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import nltk"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "nltk.download(\"book\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exploring some corpora"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the previous session we have installed the data of the collection `book`. What is that?\n",
    "\n",
    "Contrary to what you might think, this is **not** a collection of books. The name simply refers to the fact that this collection group together all the relevant NLTK modules that you need to follow along the NLTK book guide. This book is called [Natural Language Processing with Python](http://www.nltk.org/book/) and was written (and it is constatly updated) by Steven Bird, Ewan Klein, and Edward Loper.\n",
    "\n",
    "I simply cannot overstate how good this book is! It is a first-class introduction to: NLP in general, Python in general and NLTK of course. Virtually no Python is requested to follow along."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Natural Language Processing With Python](https://avatars3.githubusercontent.com/u/124114?s=400&v=4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The `book` texts"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from nltk import book"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `book` data sets packages a few useful functions and a series of sample texts. All the functions and texts are for the sake of exemplifying! All the code is taken from other modules of NLTK and is accessible also from outside the special environmente of the book tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "import matplotlib \n",
    "matplotlib.rcParams['figure.figsize'] = (12, 8)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The book module (and NLTK in general) contain also functions to generate useful calculations and especially plots. Those plots however depend on some other libraries which are not part of the standard library: `matplotlib` is the most important of them.\n",
    "\n",
    "This module (which is one of the most important for current python-based data analysis) is pre-packaged in Anaconda. If you're not using Anaconda, then use your preferred method to install it. Most likely, it will also install a lot of dependencies.\n",
    "\n",
    "The code in the previous cell simply sets different size default values for the figures generated by `matplotlib`. The standard default values are really too small..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "book.texts()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "holy_grail = book.text6\n",
    "inaugural = book.text4\n",
    "chats = book.text5\n",
    "genesis = book.text3\n",
    "sense = book.text2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Some useful functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we said yesterday, some useful instruments that allow  inspect a corpus include:\n",
    "* concordances and kwic\n",
    "* frequency lists\n",
    "* frequency distribution and dispersion\n",
    "* n-gram extraction\n",
    "\n",
    "NLTK book collects some useful functions to generate these views"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "E.g. concordances"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "type(holy_grail)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "help(chats)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "holy_grail[:100]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "inaugural.concordance(\"executive\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Contrast this with the use of democracy in the Monty Python movie. Hilarious, isn't it?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "holy_grail.concordance(\"executive\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![Monty Python Holy Grail](https://images-na.ssl-images-amazon.com/images/S/sgp-catalog-images/region_US/seeso-Monty_Python_And_The_Holy_Grail_Mov-Full-Image_GalleryBackground-en-US-1483993549331._RI_SX940_.jpg)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We said that concordances from corpora are a good way to get (at list a first sense) on the possible lexical meenings of a word. Let's see how a different corpus (an economic newspaper) use the same word \"executive\":"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "book.text7.concordance(\"executive\", lines=30)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "chats.concordance(\"lol\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Context similarity**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "help(inaugural.similar)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "chats.similar(\"god\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A nice use of this similarity detector of context is done in the NLTK book. Observe how different the usage of the world \"monstrous\" is in *Moby Dick* and *Sense and Sensibility*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "book.text1.similar(\"monstrous\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "sense.similar(\"monstrous\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "book.text1.concordance(\"monstrous\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Frequency distributions"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "help(book.FreqDist)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first thing we can do when inspecting the frequency distribution of the tokens in a text is initializing a Frequency Distribution object. We pass the text (more correctly, the list of tokens) to it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "freq = book.FreqDist(book.text5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "freq_moby = book.FreqDist(book.text1)\n",
    "freq_moby.most_common(50)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now inspect the 50 most frequent words in *Sense and Sensibility* or the English translation of the *Genesis*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "freq.most_common(50)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "len(book.text1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "freq_moby.plot(50, cumulative=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "len(chats)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How frequent is the word \"God\"?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "freq[\"LOL\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Filtering the words: vocabulary size"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Any text contained in the book module is an object of a particular type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "type(genesis)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Practically speaking, however, it is a sort of a list-like iterable of the different tokens. It is very easy to iterate over it or perform operations over each word within it. One other useful feature is that we can take this list-like object and:\n",
    "* count its elements\n",
    "* turn it into other collection type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#how do we count the number of elements in the genesis?\n",
    "len(chats)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But how many *unique* words does this text have? Obviously, many words (like \"the\", \"is\", \"God\"...) are repeated many times. How do we make sure that we count them only once?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "#hint! we have to transform this list into something else\n",
    "len(set(chats))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is particularly useful because now we are able to measure the lexical diversity or vocabulary size. How rich is the lexicon of a text? We define `lexical diversity` as the ratio between the number of unique words and the total length of a text.\n",
    "\n",
    "This is not predefined in the functions of NLTK, but it is really a piece of cake to define it, right?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def lexical_diversity(wordlist):\n",
    "    '''Let us define it! let's start with the arguments. How many arguments to we need?\n",
    "    What do we want this function to return?\n",
    "    \n",
    "    Parameters\n",
    "    ---------\n",
    "    some par : data type\n",
    "        lore ipsum\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    what do we return?\n",
    "    \n",
    "    '''\n",
    "    div = len(set(wordlist)) / len(wordlist)\n",
    "    return div\n",
    "    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "help(lexical_diversity)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "lexical_diversity(holy_grail)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(BTW1, the text in the triple quotes right after the function name is called [Docstring](https://docs.python.org/3.6/tutorial/controlflow.html#tut-docstrings). You may think of it a sort of \"gloryfied\" comment. It is very useful to document what a function does. There are different styles to write docstring. I like this one that is generally referred to as the [numpy style](https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt): it is a bit verbose, but very clear!)\n",
    "\n",
    "(BTW2, the keywork `pass` is often used in these cases: it tells the interpreter do to... exactly nothing, so that, when interpreted, this code will effectively accomplish nothing! But it won't return any syntax error either, as a completely empyt function would. For this reason it is often used when you want to leave the concrete function definition for a following step.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Excursus: list filtering and list comprehension"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the previous sections we counted the frequencies of all words in this list-like of objects that are the texts. But what if we want to filter out some of those words? After all, the counts were not very interesting...\n",
    "\n",
    "See for instance what the most frequent words are for the *Holy Grail* movie"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "freq = book.FreqDist(holy_grail)\n",
    "freq.most_common(50)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A lot of them are simply the name of the speaking characters in all caps. What if we want to filter them out? What we'd need to do is:\n",
    "\n",
    "* use a method that verifies the string case\n",
    "* loop over the elements\n",
    "* save all the elements matching our initial condition in a new list\n",
    "\n",
    "Step 1 can be performed using the `isupper` method of strings. This method returns True if string is all in uppercase; False otherwise."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "'LANCELOT'.isupper()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given the Python syntax that you already know, it is relatively easy to implement the algorithm above using the usual suspects (for loops, if statements, append method of lists, etc.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "len(holy_grail)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "noupper = []\n",
    "for w in holy_grail:\n",
    "    if w.isupper() == False:\n",
    "        noupper.append(w)\n",
    "len(noupper)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "However there is a **more pythonic way to do this**, which is both nicer, more compact to write and logically more sound. It uses a syntax called **list comprehension**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "holy_noupper = [w for w in holy_grail if w.isupper() == False]\n",
    "len(holy_noupper)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that you can nest as many list comprehensions as you want in a list comprehension. It only becomes rather clumsy already with the first nested list comprehension... But two nested lc are not impossible to manage."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NLTK book give a couple of interesting uses of FreqDist with list filtered. E.g. here are the most frequent long words (len > 7) in *Moby Dick*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "long_words = [w for w in book.text1 if len(w) >= 7 and w.isupper() == False]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "freq = book.FreqDist(long_words)\n",
    "freq.most_common(20)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "freq.plot(30)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "moby_legnths = [len(w) for w in book.text1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "freq_lengths = book.FreqDist(moby_legnths)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "freq_lengths.plot(20)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## N-grams"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(quoting from the NLTK book)\n",
    "\n",
    "```\n",
    "A collocation is a sequence of words that occur together unusually often. Thus red wine is a collocation, whereas the wine is not. A characteristic of collocations is that they are resistant to substitution with words that have similar senses; for example, maroon wine sounds definitely odd.\n",
    "\n",
    "To get a handle on collocations, we start off by extracting from a text a list of word pairs, also known as bigrams This is easily accomplished with the function bigrams():\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "for b in nltk.bigrams(\"to be or not to be that is the question\".split(\" \")):\n",
    "    print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These are just bigrams, i.e. every possible pair of tokens that can appear side by side in this short text. NLTK has a method, called `collocations`, that attempt to discover whether a certain bigram is, linguistically speaking, a collocation. Let's see how it performs with the inagural addresses of the US presidents"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "inaugural.collocations()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By the way, can we compute the frequency of the different bigrams in this texts? Let us see"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "bigs = list(nltk.bigrams(book.text4))\n",
    "freq = book.FreqDist(bigs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "freq.most_common(10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As it can be seen, the first collocation returned by the method (\"United States\") is attested:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "freq[('United', 'States')]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "times, more than 10 times less frequently then the most attested bigram. So the function did a pretty good job in guessing..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## There's more in life than what you read in a book..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we said, the cool thing is that, though it is very practical to try this functions on the sample texts, NLTK provides these functions as normal functions that you can reuse to explore your own corpus!\n",
    "\n",
    "Let see how to import them (and where to find them)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "#FreqDist\n",
    "from nltk.probability import FreqDist\n",
    "#Collocations\n",
    "from nltk import collocations\n",
    "#Bigrams\n",
    "from nltk import bigrams"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Concordances are a bit more complicated. The function we used is technically speaking a method of the nltk.Text object. Thus, before we can use it we need to instantiate an nltk.Text object (and before, obviously we need to import the class)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from nltk import Text"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us build the concordance index of the word \"Bennet\" in the snippet of *Pride and Prejudice* you had for your homework..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from nltk.tokenize import word_tokenize"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Corpora in NLTK"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The truly amazing thing in NLTK is that you can download a series of corpora and then load them using the pre-built *corpus readers*.\n",
    "\n",
    "You can download and read an incredible variety of corpora in a great variety of formats, which have been made available to the platform by their creators. They include:\n",
    "\n",
    "* corpora in simple text or XML\n",
    "* word lists\n",
    "* dictionaries\n",
    "* treebanks\n",
    "\n",
    "Furthermore, you can also use the readers to load your own corpora! Even better, it is not difficult for advanced programmers to sub-class the corpus readers in order to adapt them to their own materials."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "Let us inspect the Brown corpus that we mentioned yesterday\n",
    "\n",
    "(Note: NLTK has two versions of the Brown corpus: one simple txt, which is considered deprecated, and a TEI-XML one. We will inspect the deprecated one first)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "from nltk.corpus import brown, stopwords"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Brown corpus "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Brown, as it can be seen, is a `categorized` corpus. Let's see what it means by inspecting the different categories"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "for c in brown.categories() : print(c)\n",
    "len(brown.categories())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This method is specific for categorized corpora and not all corpora are categorized"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "len(brown.fileids(\"adventure\"))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "brown.fileids()[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "f = brown.fileids()[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "sample_path = brown.abspath(brown.fileids()[0])\n",
    "sample_path"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "print(brown.readme())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "brown.words(categories=[\"adventure\"])[0:20]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Stopwords"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "stopwords.words(\"english\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  },
  "nav_menu": {},
  "toc": {
   "navigate_menu": true,
   "number_sections": true,
   "sideBar": false,
   "threshold": 6,
   "toc_cell": false,
   "toc_section_display": "none",
   "toc_window_display": false
  },
  "toc_position": {
   "height": "255px",
   "left": "3.99432px",
   "right": "20px",
   "top": "100.994px",
   "width": "213px"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
