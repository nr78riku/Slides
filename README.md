# Slides
Greetings! In this repository you'll find the slides of the classes (Vorlesungen) and of some of the seminars where slides are used.

Slides are stored as PDF files.

## Classes
* **Session 1** (Mo, 9 Oct 17): dowload it from [here](https://git.informatik.uni-leipzig.de/introdh17-18/Slides/raw/master/Classes/Vorlesung-Session1.pdf) 
* **Session 2** (Mo, 16 Ocy 17): dowload it from [here](https://git.informatik.uni-leipzig.de/introdh17-18/Slides/raw/master/Classes/Vorlesung-Session2_161017.pdf)
* **Session 3** (Mo, 23 Ocy 17): dowload it from [here](https://git.informatik.uni-leipzig.de/introdh17-18/Slides/raw/master/Classes/Vorlesung-Session3_231017.pdf)
* **Session 4** (Mo, 30 Ocy 17): dowload it from [here](https://git.informatik.uni-leipzig.de/introdh17-18/Slides/raw/master/Classes/Vorlesung-Session4_301017.pdf)

## Seminars
* **Session 1** (Di, 9 Oct 17 / Mo, 16 Oct 17): dowload it from [here](https://git.informatik.uni-leipzig.de/introdh17-18/Slides/raw/master/Seminars/Seminar-Session1.pdf)
* **Session 2** (Di, 17 Oct 17 / Mo, 23 Oct 17): dowload it from [here](https://git.informatik.uni-leipzig.de/introdh17-18/Slides/raw/master/Seminars/Seminar-Session2_171017.pdf)
* **Session 3** (Di, 24 Oct 17 / Mo, 30 Oct 17): a markdown export of the Notebook is found [here](https://git.informatik.uni-leipzig.de/introdh17-18/Slides/raw/master/Seminars/Session3_IntroPython); the Notebook is stored [here](https://git.informatik.uni-leipzig.de/introdh17-18/Materials-Exercises/HOWTOs).
